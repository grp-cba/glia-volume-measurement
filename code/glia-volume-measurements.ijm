/** 
 * Glia volume measurements
 * 
 * Purpose:
 * - Measure the volume of glia cells
 * 
 * Dsiclaimer:
 * - Use this macro with care: If parts of the cells are very thin 
 *   and subject to the diffraction limit 
 *   we are not sure how reliably the cell's volume can be measured at all.
 * 
 * Repository:
 * - https://git.embl.de/grp-cba/glia-volume-measurement
 * 
 * 
 * Input data requirements:
 *  - A folder with 3D input data
 *  - The images may contain several channels, but only the selected channel will be analyzed
 * 	- The images must be calibrated in micrometer units
 *   - All objects will be segmented using a the same intensity threshold.
 *     This threshold must be manually determined before(!) running the analysis.
 *  - It only can open VSI and TIFF files
 *  - For VSI files only the first "image" will be analyzed (note that VSI files can contain
 *    multiple images)
 * 
 * Output:
 *   - For each image in the input folder:
 *     - 3D label mask image
 *     - Segment feature table
 * 
 * Usage:
 *   - Open Fiji
 *   - Open this file (drag and drop onto Fiji) 
 *   - Click the [ Run ] button 
 * 
 */


// INPUT PARAMETERS
//
#@ File (label="Input directory", style="directory") directory
#@ File (label="Output directory", style="directory") outputDirectory
#@ Integer (label="Intensity threshold (measure this before manually!)") threshold
#@ Integer (label="Channel to be analyzed (one-based)") channelIndex
#@ Boolean (label="Close images after analysis") closeImages

// CODE
//
print("\\Clear");
print( "# Glia Volume Analyzer" );
run("Bio-Formats Macro Extensions"); // to read multi-series (e.g. Leica lif or Olympus vsi) files
closeAll();
processFiles(directory+"/")
if (closeImages) closeAll();
selectWindow("Log");

// Functions
//
function processFiles(dir) {
	//print("Inspecting folder: " + dir);
	list = getFileList(dir);
  	for (i=0; i<list.length; i++) {
      if (endsWith(list[i], "/")) 
      {
      	  //print("Inspecting folder: "+dir+list[i]);
          processFiles(""+dir+list[i]);
      }
      else {
         //print("Found file: "+dir+list[i]);
         processFile(dir, list[i]);
      }
  }
}

function processFile(dir, fileName) {

	if( endsWith(fileName, ".vsi") || endsWith(fileName, ".tif") )
	{
		seriesIndex = 1;
		if (closeImages) closeAll();
		path = dir + fileName; 
		//print("Opening: " + path);
		run("Bio-Formats", "open=[&path] autoscale color_mode=Default view=Hyperstack stack_order=XYCZT series_"+seriesIndex);
		analyze();
	}
} 

function analyze()
{
	fileName = File.nameWithoutExtension;
	imageName = getTitle();	
	print( "\n## Analyzing file: " + fileName );
	print( "Image name: " + imageName );
	
	getPixelSize(unit, pw, ph, pd);
	print( "Pixel size: " + pw + " " + ph + " " + pd + " " + unit );
	getDimensions(width, height, channels, slices, frames);
	
	if ( channels > 1)
	{
		rename("channels");
		run("Split Channels");
		selectWindow("C"+channelIndex+"-channels"); // analyze the selected channel
	}
	
	rename("input");
	
	// select ROI
	run("Z Project...", "projection=[Max Intensity]");
	run("Enhance Contrast", "saturated=0.35");
	setTool("rectangle");
	waitForUser("Please draw a Rectangle ROI around the glia body\nor press Ctrl(Command)+A to select the whole image.");
	selectWindow("input");
	run("Restore Selection");
	run("Crop");
	
	// down sample to reduce noise and increase analysis speed
	// due to the anisotropy of the input data this will hardly affect the analysis accuracy
	// TODO: adapt this automatically to the degree of anisotropy in the input data
	run("Scale...", "x=0.5 y=0.5 interpolation=Bilinear average process create");
	rename("segmentation input");
		
	// segment and measure
	run("3D Objects Counter", "threshold=&threshold min.=10 max.=10000000000000 objects statistics summary");
	
	// check success
	Dialog.createNonBlocking("Title");
	Dialog.addMessage("Please check the segmentation quality by comparing the \"segmentation input\" and the \"Objects map of segmentation input\" images.\n\nIf the segmentation is not satisfactory click Cancel to abort this run and to manually check the data and threshold again.");
	Dialog.show();

	// output results in Log window
	for (i = 0; i < Table.size; i++) 
	{
		print( "Volume (micron^3): " + Table.get("Volume (micron^3)", i) );
	}
	
	// save label mask image
	outputPath = outputDirectory + File.separator + fileName + "_seg.tif";
	print("Saving label mask to:\n" + outputPath);
	saveAs("Tiff", outputPath);

	// save results table
	outputPath = outputDirectory + File.separator + fileName + ".csv";
	print("Saving feature table to:\n" + outputPath);
	saveAs("Results", outputPath);	
	wait(1000); // apparently needed to wait for the saving to finish before closing all windows again
}

function closeAll()
{
	run("Close All");
	windows = getList("window.titles");
	for (i = 0; i < windows.length; i++) 
	{
		if ( windows[ i ] == "Log" )
		{
			// skip	
		}
		else 
		{
			close( windows[ i ] );
		}
	}
}
