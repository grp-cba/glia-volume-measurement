# Glia Volume Measurement

This repository contains an ImageJ Macro to measure the volume of cells (glial cells) in 3D images.

The user is asked to manually provide a threshold for the cells, which has to be determined manually before running the macro. 

A maximum intensity projection of the image is shown for the user to select the Region of Interest (ROI), containing the cells.   

After selection of the ROI, the macro segments the image based on the threshold provided and then utilises the ‘3D objects counter’ plugin to measure the volume of all segments.

As output, the segmented image (label mask) along with a ‘.csv’ file containing the values for the measured parameters (Volume, Surface) for all segments is saved in the specified output folder. 


## Install and run the macro

- [Install Fiji](https://fiji.sc/)
- Download the imagej macro that can be found in this repository in the `/code` folder. [download link](https://git.embl.de/grp-almf/francesca-coraggio-glia-volume-measurement/-/raw/main/code/glia-volume-measurements.ijm?inline=false)
- Open Fiji
  - [ File > New > Script ]: Open the downloaded macro
  - Now the macro can be run by clicking the [ Run ] button
    - Please first carefully read the comment section at the top of the macro! 

## Example data

- Download two example VSI images here (TODO: download link) (currently at EMBL in `/cba/exchange/Mahak Bhushan/vsi-files-for-volume-measure`).
- The second channel contains the cell signal.
- As threshold a value of 110 was used.

## Screenshots

Screenshots using the macro with one of the above example data files.

#### Manually drawn ROI to select the cells

![""](/doc/roi.png) 

#### Output label mask and table for visual QC

![""](/doc/output.png) 

Note that the label mask and table are also stored as files.

For the published project (add publication link) we only considered the largest segment for downstream analysis and rejected the smaller ones as noise.





